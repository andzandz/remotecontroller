/* Sketch to write button presses to 4021 shift registers to control a Super Nintendo. 
 * Pins go low when they are pressed. 
 */

#define BAUD_RATE 115200

// Button pin definitions
#define PIN_A 2
#define PIN_B 3
#define PIN_X 4
#define PIN_Y 5

#define PIN_L 6
#define PIN_R 7

#define PIN_UP 8
#define PIN_DOWN 9
#define PIN_LEFT 10
#define PIN_RIGHT 13

#define PIN_START 14
#define PIN_SELECT 15

/* 1st Byte Masks */
#define _A      0b00000001
#define _X      0b00000010
#define _L      0b00000100
#define _R      0b00001000

/* 2nd Byte Masks */
#define _UP     0b00010000
#define _DOWN   0b01000000
#define _LEFT   0b00100000
#define _RIGHT  0b10000000
#define _Y      0b00000010
#define _B      0b00000001
#define _START  0b00001000
#define _SELECT 0b00000100


void setPin(int pin)
{
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
}

void setup() 
{
    Serial.begin(BAUD_RATE);
    
    setPin(PIN_A);
    setPin(PIN_B);
    setPin(PIN_X);
    setPin(PIN_Y);
    
    setPin(PIN_L);
    setPin(PIN_R);
    
    setPin(PIN_UP);
    setPin(PIN_DOWN);
    setPin(PIN_LEFT);
    setPin(PIN_RIGHT);
    
    setPin(PIN_START);
    setPin(PIN_SELECT);

}

byte firstByte = 0;
byte secondByte = 0;

void loop() 
{
  if(Serial.available() > 1)
  {
    firstByte = Serial.read();
    secondByte = Serial.read();

    /* Set first byte buttons */
    digitalWrite(PIN_A, !(firstByte & _A));
    digitalWrite(PIN_X, !(firstByte & _X));
    digitalWrite(PIN_L, !(firstByte & _L));
    digitalWrite(PIN_R, !(firstByte & _R));

    /* Set second byte buttons */
    digitalWrite(PIN_UP,    !(secondByte & _UP));
    digitalWrite(PIN_DOWN,  !(secondByte & _DOWN));
    digitalWrite(PIN_LEFT,  !(secondByte & _LEFT));
    digitalWrite(PIN_RIGHT, !(secondByte & _RIGHT));
    digitalWrite(PIN_Y,     !(secondByte & _Y));
    digitalWrite(PIN_B,     !(secondByte & _B));
    digitalWrite(PIN_START, !(secondByte & _START));
    digitalWrite(PIN_SELECT,!(secondByte & _SELECT));

  }


}
