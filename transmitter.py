import socket
import pygame

IP = "127.0.0.1"
port = 5005

# Buttons
buttons = \
    {
        1: "BUTTON_A",
        2: "BUTTON_B",
        3: "BUTTON_Y",
        0: "BUTTON_X",
        5: "SHOULDER_RIGHT",
        4: "SHOULDER_LEFT",
        9: "BUTTON_START",
        8: "BUTTON_SELECT"
    }

upDownAxis = 1
leftRightAxis = 0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send(msg):
    msg = bytes(msg, encoding='utf-8')
    sock.sendto(msg, (IP, port))


def main():

    pygame.init()
    pygame.display.set_mode((200,200))
    print("Firing button presses at: {}:{}".format(IP, port))
    pygame.display.set_caption("{}:{}".format(IP,port))

    joystick_count = pygame.joystick.get_count()
    if joystick_count < 1:
        raise Exception("No gamepads found")

    joy = pygame.joystick.Joystick(0)
    joy.init()

    # Become true when the button on the axis is pressed
    # So we know which button has come back up when the axis position returns to 0
    upIsDown = False
    downIsDown = False
    leftIsDown = False
    rightIsDown = False

    while True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

            if event.type == pygame.JOYBUTTONDOWN:
                #print("{}_DOWN".format(buttons[event.button]))
                send("{}_DOWN".format(buttons[event.button]))


            if event.type == pygame.JOYBUTTONUP:
                #print("{}_UP".format(buttons[event.button]))
                send("{}_UP".format(buttons[event.button]))

            if event.type == pygame.JOYAXISMOTION:
                # If the event is on the up/down axis
                if event.axis == upDownAxis:
                    if event.value == 0:
                        if upIsDown:
                            upIsDown = False
                            send("HAT_UP_UP")
                        elif downIsDown:
                            downIsDown = False
                            send("HAT_DOWN_UP")
                    elif event.value > 0:
                        send("HAT_DOWN_DOWN")
                        downIsDown = True
                    elif event.value < 0:
                        send("HAT_UP_DOWN")
                        upIsDown = True

                # If it's left/right axis
                if event.axis == leftRightAxis:
                    if event.value == 0:
                        if leftIsDown:
                            leftIsDown = False
                            send("HAT_LEFT_UP")
                        elif rightIsDown:
                            rightIsDown = False
                            send("HAT_RIGHT_UP")
                    elif event.value > 0:
                        send("HAT_RIGHT_DOWN")
                        rightIsDown = True
                    elif event.value < 0:
                        send("HAT_LEFT_DOWN")
                        leftIsDown = True


if __name__ == "__main__":
    main()